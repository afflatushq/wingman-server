(defproject wingman-server "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :aot :all
  :main wingman-server.core 
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [http-kit "2.1.18"] 
                 [cheshire "5.6.1"]
                 [prismatic/schema "1.1.1"]
                 [com.apa512/rethinkdb "0.15.23"]
                 [com.taoensso/carmine "2.12.2"]])
