(ns wingman-server.core
  (:gen-class)
  (:use org.httpkit.server
        wingman-server.types)
  (:require [cheshire.core :refer :all]
            [schema.coerce :as coerce]
            [rethinkdb.query :as r]))

(def channel-map (atom {}))

(defmacro rethink-> [& args]
  `(with-open [conn# (r/connect :host "127.0.0.1" :port 28015 :db "test")]
     (-> ~@args
         (r/run conn#))))

(defn not-eq [term1 term2]
  (r/not (r/eq term1 term2)))

(defmulti process-message
  "Processes the messages from the client"
  :message-type)

(defmethod process-message "handshake"
  [msg channel]
  (swap! channel-map assoc (:user-id msg) channel))

(defmethod process-message "send-message"
  [msg channel]
  (let [to-channel (get @channel-map (:recipient msg))]
    (send! to-channel (generate-string msg))))

(defmethod process-message "update-loc"
  [msg channel]
  (let [{{lat :lat long :long} :pos
         user-id :user-id} (parse-global-update msg)]
    (rethink-> (r/db "test")
               (r/table "users")
               (r/get-all [name] {:index "name"})
               (r/update (r/fn [user]
                           {:location (r/point lat long)})))))

(defmethod process-message "get-nearby"
  [msg channel]
  (let [{{lat :lat long :long} :pos
         user-id :user-id} (parse-global-update msg)]
    (rethink-> (r/db "test")
               (r/table "users")
               (r/get-nearest (r/point lat long) {:index "location" :max-results 25})
               (r/filter (r/fn [person]
                           (not-eq name (r/get-field person "userId")))))))

(defmethod process-message "send-loc"
  [msg channel]
  (let [msg (parse-direct-update msg)
        to-channel (get (:to msg) @channel-map)   ; get the person you're walking to.
        global-update (direct->global-update msg) ; get a global update
        direct-update (generate-string msg)]      ; serialize the data to send to the other person.
    (process-message global-update)               ; update your position globally
    (send! to-channel direct-update)))            ; send your location to the other person.

(defn handle-receive [msg channel]
  (when-let [msg (parse-string msg true)]         ; if the string is valid json, then handle it.
    (process-message msg channel)))

(defn handler [request]
  (with-channel request channel
    (on-close channel (fn [status]
                        (println "channel closed: " status "\t" "channel:" channel)))
    (on-receive channel #(handle-receive % channel))))

(defn -main []
  (run-server handler {:port 9090}))
