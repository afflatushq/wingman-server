(ns wingman-server.utils)

(defn filter-val [value hm]
  (reduce-kv (fn [acc k v]
               (if (= value v)
                 (conj acc k)
                 acc))
             #{} hm))

(defn dissoc-val [hm value]
  (-> (filter-val value hm)
      (->> (apply dissoc hm))))
