(ns wingman-server.types
  (:require [schema.core :as s]
            [schema.coerce :as coerce]))

(def GlobalUpdateLocationReq
  {:message-type String
   :name String
   :pos {:lat  double
         :long double}})

(def parse-global-update
  (coerce/coercer GlobalUpdateLocationReq coerce/json-coercion-matcher))

(def DirectUpdateLocationReq
  (assoc GlobalUpdateLocationReq :to String))

(def parse-direct-update
  (coerce/coercer DirectUpdateLocationReq coerce/json-coercion-matcher))

(defn direct->global-update [u]
  (-> u
      (dissoc :to)
      (assoc :message-type "geolocation-update")))
